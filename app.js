var express     = require('express');
var app         = express();
var mongoose    = require('mongoose');
var flowRoute   = require('./routes/flow');
var bodyParser  = require('body-parser');
var engine      = require('ejs-mate');

mongoose.connect('mongodb://localhost/build', function(err) {
   if(err) {
       console.log('connection error', err);
   } else {
       console.log('connection successful');
   }
});

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use('/flow', flowRoute);

app.engine('ejs', engine);
app.set('view engine', 'ejs');
app.set('views',__dirname + '/views');

var port = process.env.PORT || 3000;

app.listen(port);
console.log('Magic happens on port ' + port);
