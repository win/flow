var mongoose = require('mongoose');

var metricsDef = {
    test: Number,
    maintability: Number,
    security: Number,
    workmanship: Number
};

var flowSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true
    },
    owner: String,
    timeStarted: {
        type: Date,
        default: Date.now()
    },
    state: String,
    build: String,
    unitTest: Number,
    functionalTest: Number,
    metrics: metricsDef
});

module.exports = mongoose.model('flow', flowSchema);

/*db.flows.insert({
 name: "42114",
 owner: "johny",
 timeStarted: "4/17/2014 11:12 pm",
 state: "Accepted",
 build: "debug",
 unitTest: 65,
 functionalTest: 63,
 metrics: {
 test: 64,
 maintability: 53,
 security: 52,
 workmanship: 50
 }
 });*/