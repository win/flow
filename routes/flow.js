/*
 * GET home page.
 */
var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var flow = require('../models/flow.js');

router.get('/', function(req, res, next) {
    flow.find(function(err, flows) {
        if(err) return next(err);
        /*res.render('flow/index', {
            flows: flows,
            title: 'Flows'
        });*/
        res.json(flows);
    });
});

module.exports = router;